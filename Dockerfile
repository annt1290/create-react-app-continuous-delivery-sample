FROM node:8

RUN npm install -g serve

COPY . /app

WORKDIR /app

RUN npm install

RUN npm run build

EXPOSE 5000

CMD [ "serve", "-s", "build" ]
